package com.deveddy.seriesguidefilelistextension

import com.battlelancer.seriesguide.api.Episode
import com.battlelancer.seriesguide.api.Movie
import com.deveddy.seriesguidefilelistextension.preferences.AppPreferences
import java.net.URLEncoder
import java.util.Calendar
import java.util.Date

class SeriesGuideDomainMapper(private val appPreferences: AppPreferences) {

    fun mapToQuery(episode: Episode): String {
        return "https://filelist.io/browse.php?search=${URLEncoder.encode(name(episode), "UTF-8")}"
    }

    fun name(episode: Episode): String = episode.run {
        val title = this.showTitle.stripSymbols()
        val season = this.season.toString().padStart(2, '0')
        val ep = this.number.toString().padStart(2, '0')
        "$title s${season}e$ep ${qualityQueryParameter()}"
    }

    private fun qualityQueryParameter(): String {
        return when (val parameter = appPreferences.quality()) {
            "none" -> ""
            else -> parameter
        }
    }

    fun mapToQuery(movie: Movie): String {
        return "https://filelist.io/browse.php?search=${URLEncoder.encode(name(movie), "UTF-8")}"
    }

    fun name(movie: Movie): String = movie.run {
        "${this.title.stripSymbols()} ${this.releaseDate.calendarYear()} ${qualityQueryParameter()}"
    }

    private fun Date.calendarYear(): Int {
        val calendar = Calendar.getInstance()
        calendar.time = this
        return calendar.get(Calendar.YEAR)
    }

    private fun String.stripSymbols(): String {
        val symbolsToStrip: String = appPreferences.stripSymbols()
        return symbolsToStrip.fold(this) { acc, char ->
            acc.replace(char.toString(), "")
        }
    }
}
