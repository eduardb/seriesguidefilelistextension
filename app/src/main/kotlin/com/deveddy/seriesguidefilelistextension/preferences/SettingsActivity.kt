package com.deveddy.seriesguidefilelistextension.preferences

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat
import com.deveddy.seriesguidefilelistextension.R
import com.deveddy.seriesguidefilelistextension.toUri

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(
                        R.id.settings,
                        SettingsFragment()
                )
                .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.how_to_get_the_passkey -> {
                showHowToGetThePasskeyDialog()
                return true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun showHowToGetThePasskeyDialog() {
        with(AlertDialog.Builder(this)) {
            setMessage(R.string.go_to_profile_and_copy_passkey)
            setNegativeButton(android.R.string.cancel) { _, _ ->
                // do nothing
            }
            setPositiveButton(R.string.go_to_profile) { _, _ ->
                startActivity(Intent(Intent.ACTION_VIEW, "https://filelist.io/my.php".toUri()))
            }
            create()
        }.show()
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
        }
    }
}
