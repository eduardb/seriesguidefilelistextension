package com.deveddy.seriesguidefilelistextension.preferences

import android.content.Context
import androidx.preference.PreferenceManager
import com.deveddy.seriesguidefilelistextension.R

class AppPreferences(context: Context) {

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    private val keyQuality = context.getString(R.string.pref_key_quality)
    private val defaultQuality = context.getString(R.string.pref_default_quality)

    private val keyStripSymbols = context.getString(R.string.pref_key_strip_symbols)
    private val defaultStripSymbols = context.getString(R.string.pref_default_strip_symbols)

    private val keyUseInAppSearch = context.getString(R.string.pref_key_use_in_app_search)
    private val defaultUseInAppSearch = context.resources.getBoolean(R.bool.pref_default_use_in_app_search)

    fun quality(): String = preferences.getString(keyQuality, defaultQuality) ?: defaultQuality

    fun stripSymbols(): String = preferences.getString(keyStripSymbols, defaultStripSymbols) ?: defaultStripSymbols

    fun useInAppSearch(): Boolean = preferences.getBoolean(keyUseInAppSearch, defaultUseInAppSearch)

}
