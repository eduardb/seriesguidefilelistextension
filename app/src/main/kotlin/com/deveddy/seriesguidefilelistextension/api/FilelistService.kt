package com.deveddy.seriesguidefilelistextension.api

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query

interface FilelistService {

    interface ErrorCodes {
        companion object {
            const val INVALID_SEARCH_OR_FILTER = 400
            const val EMPTY_USERNAME_AND_PASSKEY = 401
            const val INVALID_USERNAME_OR_PASSKEY = 403
            const val RATE_LIMIT_REACHED = 429
            const val SERVICE_UNAVAILABLE = 503
        }
    }

    @GET("api.php?action=search-torrents&type=name")
    suspend fun searchTorrents(@Query("query") query: String): List<TorrentDto>
}

@Suppress("ConstructorParameterNaming")
@Parcelize
@Keep
data class TorrentDto(
        val id: Long,
        val name: String,
        val imdb: String?,
        val freeleech: Int,
        val doubleup: Int,
        val upload_date: String,
        val download_link: String,
        val size: Long,
        val internal: Int,
        val moderated: Int,
        val category: String,
        val seeders: Int,
        val leechers: Int,
        val times_completed: Int,
        val comments: Int,
        val files: Int,
        val small_description: String,
        val tv: TvDto?
) : Parcelable

@Parcelize
data class TvDto(
        val season: String,
        val episode: String
) : Parcelable


class FilelistServiceModule(retrofit: Retrofit) {

    internal val filelistService: FilelistService = retrofit.create(FilelistService::class.java)
}
