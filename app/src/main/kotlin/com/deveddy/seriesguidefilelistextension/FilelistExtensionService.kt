package com.deveddy.seriesguidefilelistextension

import android.content.Context
import android.content.Intent
import com.battlelancer.seriesguide.api.Action
import com.battlelancer.seriesguide.api.Episode
import com.battlelancer.seriesguide.api.Movie
import com.battlelancer.seriesguide.api.SeriesGuideExtension
import com.deveddy.seriesguidefilelistextension.preferences.AppPreferences
import com.deveddy.seriesguidefilelistextension.search.SearchActivity

fun title(context: Context) = context.getString(R.string.extension_label)

open class FilelistExtensionService : SeriesGuideExtension("Filelist Extension") {

    private lateinit var mapper: SeriesGuideDomainMapper
    private lateinit var appPreferences: AppPreferences

    override fun onCreate() {
        super.onCreate()
        mapper = appComponent.domainMapper()
        appPreferences = appComponent.appPreferences()
    }

    override fun onRequest(episodeIdentifier: Int, episode: Episode) {
        publishEpisodeFilelistAction(episodeIdentifier, episode)
    }

    override fun onRequest(movieIdentifier: Int, movie: Movie) {
        publishMovieFilelistAction(movieIdentifier, movie)
    }

    private fun publishEpisodeFilelistAction(episodeIdentifier: Int, episode: Episode) =
            publishAction(
                    Action.Builder(title(this), episodeIdentifier)
                            .viewIntent(episodeIntent(episode))
                            .build()
            )

    private fun episodeIntent(episode: Episode): Intent =
            if (appPreferences.useInAppSearch()) {
                SearchActivity.searchEpisodeIntent(applicationContext, episode)
            } else {
                Intent(Intent.ACTION_VIEW)
                        .setData(mapper.mapToQuery(episode).toUri())
            }

    private fun publishMovieFilelistAction(movieIdentifier: Int, movie: Movie) = publishAction(
            Action.Builder(title(this), movieIdentifier)
                    .viewIntent(movieIntent(movie))
                    .build()
    )

    private fun movieIntent(movie: Movie): Intent =
            if (appPreferences.useInAppSearch()) {
                SearchActivity.searchMovieIntent(applicationContext, movie)
            } else {
                Intent(Intent.ACTION_VIEW)
                        .setData(mapper.mapToQuery(movie).toUri())
            }
}
