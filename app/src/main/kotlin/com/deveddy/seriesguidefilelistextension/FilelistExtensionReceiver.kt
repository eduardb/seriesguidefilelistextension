package com.deveddy.seriesguidefilelistextension

import com.battlelancer.seriesguide.api.SeriesGuideExtension
import com.battlelancer.seriesguide.api.SeriesGuideExtensionReceiver

class FilelistExtensionReceiver : SeriesGuideExtensionReceiver() {

    override fun getExtensionClass(): Class<out SeriesGuideExtension> = FilelistExtensionService::class.java

    override fun getJobId(): Int = 1
}
