package com.deveddy.seriesguidefilelistextension.search

import android.os.Parcelable
import androidx.compose.runtime.Immutable
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.battlelancer.seriesguide.api.Episode
import com.battlelancer.seriesguide.api.Movie
import com.deveddy.seriesguidefilelistextension.SeriesGuideDomainMapper
import com.deveddy.seriesguidefilelistextension.api.FilelistService
import com.deveddy.seriesguidefilelistextension.api.TorrentDto
import kotlinx.parcelize.Parcelize
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container


class SearchViewModel(
    private val mapper: SeriesGuideDomainMapper,
    private val searchService: FilelistService,
    private val navigator: Navigator,
    episode: Episode?,
    movie: Movie?,
    savedStateHandle: SavedStateHandle
) : ViewModel(), ContainerHost<SearchState, SearchSideEffect> {

    override val container =
        container<SearchState, SearchSideEffect>(
            initialState(episode, movie),
            savedStateHandle
        ) { search(it) }

    private fun initialState(
        episode: Episode?,
        movie: Movie?,
    ): SearchState {
        if ((episode != null && movie != null)) {
            throw IllegalArgumentException("Only episode or movie should be not null")
        }
        episode?.let {
            return SearchState.Searching(mapper.name(it))
        }
        movie?.let {
            return SearchState.Searching(mapper.name(it))
        }
        throw IllegalArgumentException("Both episode and movie were null")
    }

    private fun search(searchState: SearchState): Unit = intent(/*registerIdling = false*/) {
        kotlin.runCatching {
            searchService.searchTorrents(searchState.title)
        }.onSuccess { results ->
            reduce {
                SearchState.ResultsLoaded(state.title, results)
            }
        }.onFailure { ex ->
            ex.printStackTrace()
            reduce {
                SearchState.Error(state.title, ex.localizedMessage)
            }
        }
    }

    fun onSettingsClicked() = intent {
        navigator.openSettings()
    }

    fun onResultClicked(result: TorrentDto) = intent {
        if (!navigator.open(result)) {
            reduce {
                when (val it = state) {
                    is SearchState.ResultsLoaded -> it.copy(showingTorrentAppNotFoundDialog=true)
                    else -> throw IllegalStateException("state is ${state.javaClass}, expected ResultsLoaded")
                }
            }
        }
    }

    fun onOpenBrowserClicked(result: TorrentDto) = intent {
        navigator.openInBrowser(result)
    }

    fun onTorrentAppNotFoundDialogDismissed() = intent {
        reduce {
            when (val it = state) {
                is SearchState.ResultsLoaded -> it.copy(showingTorrentAppNotFoundDialog=false)
                else -> throw IllegalStateException("state is ${state.javaClass}, expected ResultsLoaded")
            }
        }
    }

    fun onTorrentAppNotFoundDialogConfirmed() = intent {
        reduce {
            when (val it = state) {
                is SearchState.ResultsLoaded -> it.copy(showingTorrentAppNotFoundDialog=false)
                else -> throw IllegalStateException("state is ${state.javaClass}, expected ResultsLoaded")
            }
        }
        navigator.searchTorrentApp()
    }
}

interface Navigator {
    fun openSettings()
    fun open(torrent: TorrentDto): Boolean
    fun openInBrowser(torrent: TorrentDto)
    fun searchTorrentApp()
}

@Immutable
sealed class SearchState(open val title: String) : Parcelable {
    @Parcelize
    @Immutable
    data class Searching(override val title: String) : SearchState(title)

    @Parcelize
    @Immutable
    data class ResultsLoaded(
        override val title: String,
        val results: List<TorrentDto> = emptyList(),
        val showingTorrentAppNotFoundDialog: Boolean = false
    ) :
        SearchState(title)

    @Parcelize
    @Immutable
    data class Error(override val title: String, val errorMessage: String?) : SearchState(title)
}

sealed interface SearchSideEffect {
    object NavigateToSettings : SearchSideEffect
}
