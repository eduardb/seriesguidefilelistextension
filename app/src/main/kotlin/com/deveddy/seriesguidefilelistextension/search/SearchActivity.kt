package com.deveddy.seriesguidefilelistextension.search

import android.content.Context
import android.content.Intent
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.battlelancer.seriesguide.api.Episode
import com.battlelancer.seriesguide.api.Movie
import com.deveddy.seriesguidefilelistextension.api.TorrentDto
import com.deveddy.seriesguidefilelistextension.appComponent
import com.deveddy.seriesguidefilelistextension.ext.makeViewModel
import com.deveddy.seriesguidefilelistextension.preferences.SettingsActivity
import com.deveddy.seriesguidefilelistextension.search.theme.AppTheme
import kotlinx.coroutines.launch


class SearchActivity : ComponentActivity() {

    companion object {
        private const val SEARCH_KIND = "SEARCH_KIND"
        private const val EPISODE = "EPISODE"
        private const val MOVIE = "MOVIE"

        fun searchEpisodeIntent(context: Context, episode: Episode): Intent {
            return Intent(context, SearchActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                putExtra(SEARCH_KIND, EPISODE)
                putExtras(episode.toBundle())
            }
        }

        fun searchMovieIntent(context: Context, movie: Movie): Intent {
            return Intent(context, SearchActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                putExtra(SEARCH_KIND, MOVIE)
                putExtras(movie.toBundle())
            }
        }
    }

    private val kind by lazy { requireNotNull(intent.getStringExtra(SEARCH_KIND)) }
    private val episode by lazy { if (kind == EPISODE) Episode.fromBundle(intent.extras) else null }
    private val movie by lazy { if (kind == MOVIE) Movie.fromBundle(intent.extras) else null }

    private val vm by makeViewModel { handle ->
        SearchViewModel(
            appComponent.domainMapper(),
            appComponent.filelistService(),
            navigator,
            episode,
            movie,
            handle,
        )
    }

    private val navigator = object : Navigator {
        override fun openSettings() {
            startActivity(
                Intent(
                    this@SearchActivity,
                    SettingsActivity::class.java
                )
            )
        }

        override fun open(torrent: TorrentDto): Boolean {
            val intent = Intent(Intent.ACTION_VIEW)
                .setDataAndType(
                    Uri.parse(torrent.download_link),
                    "application/x-bittorrent"
                )
//                .addCategory(Intent.CATEGORY_BROWSABLE)

            val chooser: Intent = Intent.createChooser(intent, "Open this torrent with")

            // Verify the original intent will resolve to at least one activity
            return if (intent.resolveActivity(packageManager) != null) {
                startActivity(chooser)
                true
            } else {
                false
            }
        }

        override fun openInBrowser(torrent: TorrentDto) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://filelist.io/details.php?id=${torrent.id}")
                )
            )
        }

        override fun searchTorrentApp() {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://search?q=torrent+client")
                )
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            AppTheme {
                val state by vm.container.stateFlow.collectAsState()

//                vm.collectSideEffect(sideEffect = ::handleSideEffect)
                LaunchedEffect(vm) {
                    launch {
                        vm.container.sideEffectFlow.collect(::handleSideEffect)
                    }
                }

                SearchScreen(
                    state = state,
                    onSettingsClicked = vm::onSettingsClicked,
                    onResultClicked = vm::onResultClicked,
                    onOpenBrowserClicked = vm::onOpenBrowserClicked,
                    onTorrentAppNotFoundDialogDismissed = vm::onTorrentAppNotFoundDialogDismissed,
                    onTorrentAppNotFoundDialogConfirmed = vm::onTorrentAppNotFoundDialogConfirmed,
                )

                /*
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {

                }
                 */
            }
        }
    }

    private suspend fun handleSideEffect(sideEffect: SearchSideEffect) {
        when (sideEffect) {
            is SearchSideEffect.NavigateToSettings -> startActivity(
                Intent(
                    this@SearchActivity,
                    SettingsActivity::class.java
                )
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchScreen(
    state: SearchState,
    onSettingsClicked: () -> Unit = {},
    onResultClicked: (TorrentDto) -> Unit = {},
    onOpenBrowserClicked: (TorrentDto) -> Unit = {},
    onTorrentAppNotFoundDialogDismissed: () -> Unit = {},
    onTorrentAppNotFoundDialogConfirmed: () -> Unit = {},
) {
    Scaffold(
        topBar = {
            SmallTopAppBar(title = { Text(state.title) })
        },
        floatingActionButton = {
            FloatingActionButton(onClick = onSettingsClicked) {
                Icon(Icons.Filled.Settings, contentDescription = "Settings")
            }
        }) {
        when (state) {
            is SearchState.Searching -> Searching()
            is SearchState.Error -> Error(state)
            is SearchState.ResultsLoaded -> ResultsLoaded(
                state,
                onResultClicked,
                onOpenBrowserClicked,
                onTorrentAppNotFoundDialogDismissed,
                onTorrentAppNotFoundDialogConfirmed
            )
        }
    }
}

@Composable
fun Searching() {
    Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            CircularProgressIndicator()
            Text(text = "Searching…")
        }
    }
}

@Composable
fun Error(state: SearchState.Error) {
    Text(text = "Error: ${state.errorMessage}")
}

@Composable
fun ResultsLoaded(
    state: SearchState.ResultsLoaded,
    onResultClicked: (TorrentDto) -> Unit = {},
    onOpenBrowserClicked: (TorrentDto) -> Unit = {},
    onTorrentAppNotFoundDialogDismissed: () -> Unit = {},
    onTorrentAppNotFoundDialogConfirmed: () -> Unit = {},
) {
    LazyColumn(modifier = Modifier.padding(8.dp)) {
        item {
            Text(text = "Found ${state.results.size} results")
        }
        items(state.results, key = { it.id }) {
            ResultItem(
                it,
                modifier = Modifier.padding(vertical = 4.dp),
                onClick = onResultClicked,
                onOpenBrowser = onOpenBrowserClicked,
            )
        }
    }
    if (state.showingTorrentAppNotFoundDialog) {
        AlertDialog(
            onDismissRequest = onTorrentAppNotFoundDialogDismissed,
            confirmButton = {
                Button(onClick = onTorrentAppNotFoundDialogConfirmed) {
                    Text(
                        stringResource(id = android.R.string.ok)
                    )
                }
            },
            dismissButton = {
                Button(onClick = onTorrentAppNotFoundDialogDismissed) {
                    Text(
                        stringResource(id = android.R.string.cancel)
                    )
                }
            },
            text = { Text("No compatible torrent client found. Go to Play Store to download one?") }
        )
    }
}


@Preview(showBackground = true, group = "Searching")
@Composable
fun SearchingLightPreview() {
    AppTheme {
        SearchScreen(SearchState.Searching("test"))
    }
}

@Preview(
    showBackground = true,
    uiMode = UI_MODE_NIGHT_YES,
    group = "Searching"
)
@Composable
fun SearchingDarkPreview() {
    AppTheme {
        SearchScreen(SearchState.Searching("test"))
    }
}

@Preview(showBackground = true, group = "ResultsLoaded")
@Composable
fun ResultsLoadedightPreview() {
    AppTheme {
        SearchScreen(
            SearchState.ResultsLoaded(
                "test", listOf(
                    TorrentDto(
                        id = 1,
                        name = "Name",
                        imdb = null,
                        freeleech = 1,
                        doubleup = 1,
                        upload_date = "date time string",
                        download_link = "link",
                        size = 2_000_000L,
                        internal = 1,
                        moderated = 1,
                        category = "cat",
                        seeders = 1234,
                        leechers = 321,
                        times_completed = 444,
                        comments = 12,
                        files = 11,
                        small_description = "descr",
                        tv = null,
                    )
                )
            )
        )
    }
}

@Preview(
    showBackground = true,
    uiMode = UI_MODE_NIGHT_YES,
    group = "ResultsLoaded"
)
@Composable
fun ResultsLoadedDarkPreview() {
    AppTheme {
        SearchScreen(
            SearchState.ResultsLoaded(
                "test", listOf(
                    TorrentDto(
                        id = 1,
                        name = "Name",
                        imdb = null,
                        freeleech = 1,
                        doubleup = 1,
                        upload_date = "date time string",
                        download_link = "link",
                        size = 2_000_000L,
                        internal = 1,
                        moderated = 1,
                        category = "cat",
                        seeders = 1234,
                        leechers = 321,
                        times_completed = 444,
                        comments = 12,
                        files = 11,
                        small_description = "descr",
                        tv = null,
                    )
                )
            )
        )
    }
}
