package com.deveddy.seriesguidefilelistextension.search

import android.content.Context
import android.content.res.Configuration
import android.text.format.Formatter
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.rememberImagePainter
import com.deveddy.seriesguidefilelistextension.R
import com.deveddy.seriesguidefilelistextension.api.TorrentDto
import com.deveddy.seriesguidefilelistextension.search.theme.AppTheme


@Suppress("MagicNumber", "LongMethod")
@Composable
fun ResultItem(
    item: TorrentDto,
    modifier: Modifier = Modifier,
    onClick: (TorrentDto) -> Unit = {},
    onOpenBrowser: (TorrentDto) -> Unit = {},
) {
    ConstraintLayout(
        modifier = modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(4.dp))
            .background(MaterialTheme.colorScheme.surfaceVariant)
            .clickable {
                onClick(item)
            }
            .padding(8.dp)
    ) {
        val (category, name, menu, descr, col1, col2, col3, col4) = createRefs()
        CategoryIcon(item.category, modifier = Modifier.constrainAs(category) {
            top.linkTo(parent.top)
            start.linkTo(parent.start)
        })
        TorrentName(item.name, modifier = Modifier.constrainAs(name) {
            top.linkTo(parent.top)
            end.linkTo(menu.start)
            start.linkTo(category.end, margin = 8.dp)
            width = Dimension.fillToConstraints
        })
        ContextMenu(
            modifier = Modifier
                .constrainAs(menu) {
                    top.linkTo(parent.top)
                    end.linkTo(parent.end)
                },
            onOpenBrowserClicked = { onOpenBrowser(item) },
        )
        TorrentMainAttributes(
            description = item.small_description,
            isInternal = item.internal == 1,
            isFreeleech = item.freeleech == 1,
            isDoubleup = item.doubleup == 1,
            modifier = Modifier.constrainAs(descr) {
                top.linkTo(name.bottom)
                end.linkTo(parent.end)
                start.linkTo(name.start)
                width = Dimension.fillToConstraints
            })
        Column(modifier = Modifier.constrainAs(col1) {
            top.linkTo(category.bottom, margin = 4.dp)
            start.linkTo(parent.start)
            width = Dimension.wrapContent
        }) {
            InfoBox(
                text = item.seeders.toString(),
                icon = R.drawable.ic_file_upload_black_16dp,
                iconTintColor = Color(0xFF78A42B),
            )
            InfoBox(
                text = item.leechers.toString(),
                icon = R.drawable.ic_file_download_black_16dp,
                iconTintColor = Color(0xFFA61122),
            )
        }
        Column(modifier = Modifier.constrainAs(col2) {
            top.linkTo(col1.top)
            start.linkTo(name.start)
            width = Dimension.wrapContent
        }) {
            InfoBox(
                text = item.size.toSize(LocalContext.current),
                icon = R.drawable.ic_cloud_download_black_16dp,
                iconTintColor = Color(0xFF2388D7),
            )
            InfoBox(
                text = item.files.toString(),
                icon = R.drawable.list_black_16dp,
                iconTintColor = Color(0xFF6AA653),
            )
        }
        Column(modifier = Modifier.constrainAs(col3) {
            top.linkTo(col2.top)
            start.linkTo(col2.end, margin = 8.dp)
            width = Dimension.wrapContent
        }) {
            InfoBox(
                text = item.times_completed.toString(),
                icon = R.drawable.ic_sync_black_16dp,
                iconTintColor = Color(0xFF3881AD),
            )
            InfoBox(
                text = item.comments.toString(),
                icon = R.drawable.message_black_16dp,
                iconTintColor = Color(0xFF2388D7),
            )
        }
        InfoBox(
            text = item.upload_date,
            icon = R.drawable.ic_today_black_16dp,
            iconTintColor = Color(0xFF80D711),
            modifier = Modifier.constrainAs(col4) {
                top.linkTo(col3.top)
                bottom.linkTo(col3.bottom)
//                start.linkTo(col3.end, margin = 18.dp)
                end.linkTo(parent.end)
                width = Dimension.value(120.dp)
            }
        )
    }
}

@Composable
fun CategoryIcon(category: String, modifier: Modifier = Modifier) {
    Image(
        painter = rememberImagePainter("https://filelist.io/styles/images/cat/hdtv.png"),
        contentDescription = category,
        modifier = modifier.size(48.dp)
    )
}

@Composable
fun TorrentName(name: String, modifier: Modifier = Modifier) {
    Text(
        text = name,
        modifier = modifier,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        style = MaterialTheme.typography.titleMedium,
    )
}

@Composable
fun TorrentMainAttributes(
    description: String,
    isInternal: Boolean,
    isFreeleech: Boolean,
    isDoubleup: Boolean,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(4.dp),
    ) {
        if (isInternal) {
            Image(
                painter = rememberImagePainter("https://filelist.io/styles/images/tags/internal.png"),
                contentDescription = "internal",
                modifier = Modifier.size(width = 60.dp, height = 15.dp)
            )
        }
        if (isFreeleech) {
            Image(
                painter = rememberImagePainter("https://filelist.io/styles/images/tags/freeleech.png"),
                contentDescription = "freeleech",
                modifier = Modifier.size(width = 60.dp, height = 15.dp)
            )
        }
        if (isDoubleup) {
            Image(
                painter = rememberImagePainter("https://filelist.io/styles/images/tags/doubleup.png"),
                contentDescription = "double up",
                modifier = Modifier.size(width = 60.dp, height = 15.dp)
            )
        }
        Text(
            text = description,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.bodyMedium,
        )
    }
}

@Composable
fun ContextMenu(modifier: Modifier = Modifier, onOpenBrowserClicked: () -> Unit = {}) {
    Box(modifier = modifier) {
        var menuExpanded by remember { mutableStateOf(false) }
        Image(
            imageVector = Icons.Filled.MoreVert,
            contentDescription = "menu",
            colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onSurfaceVariant),
            modifier = Modifier.clickable { menuExpanded = true }
        )
        DropdownMenu(expanded = menuExpanded, onDismissRequest = { menuExpanded = false }) {
            DropdownMenuItem(onClick = {
                onOpenBrowserClicked()
                menuExpanded = false
            }) {
                Text("Open in browser")
            }
        }
    }
}

private fun Long.toSize(context: Context): String {
    return Formatter.formatFileSize(context, this)
}

@Composable
fun InfoBox(
    text: String,
    @DrawableRes icon: Int,
    iconTintColor: Color,
    modifier: Modifier = Modifier
) {
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically) {
        Image(
            painter = painterResource(id = icon),
            colorFilter = ColorFilter.tint(iconTintColor),
            contentDescription = null
        )
        Text(text, modifier = Modifier.padding(start = 4.dp))
    }
}

@Preview(showBackground = true)
@Composable
fun ResultItemPreview() {
    AppTheme {
        ResultItem(
            TorrentDto(
                id = 1,
                name = "Name",
                imdb = null,
                freeleech = 1,
                doubleup = 1,
                upload_date = "date time string",
                download_link = "link",
                size = 2_000_000L,
                internal = 1,
                moderated = 1,
                category = "cat",
                seeders = 1234,
                leechers = 321,
                times_completed = 444,
                comments = 12,
                files = 11,
                small_description = "genre 1, genre 2, genre 3",
                tv = null,
            )
        )
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun ResultItemDarkPreview() {
    AppTheme {
        ResultItem(
            TorrentDto(
                id = 1,
                name = "Name",
                imdb = null,
                freeleech = 1,
                doubleup = 1,
                upload_date = "date time string",
                download_link = "link",
                size = 2_000_000L,
                internal = 1,
                moderated = 1,
                category = "cat",
                seeders = 1234,
                leechers = 321,
                times_completed = 444,
                comments = 12,
                files = 11,
                small_description = "genre 1, genre 2, genre 3",
                tv = null,
            )
        )
    }
}

@Preview(showBackground = true)
@Composable
fun InfoBoxPreview() {
    InfoBox("12", R.drawable.ic_file_upload_black_16dp, iconTintColor = Color(0xFF78A42B))
}
