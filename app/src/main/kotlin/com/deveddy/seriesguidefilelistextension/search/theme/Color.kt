package com.deveddy.seriesguidefilelistextension.search.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val Blue = Color(0xFF2D3D50)
val DarkBlue = Color(0xFF22313F)
val ContentBlue = Color(0xFF252E38)
val LightBlue = Color(0xFF34485E)
val TextColor = Color(0xFF8B9AAA)
val Background = Color(0xFFB3B4AB)
