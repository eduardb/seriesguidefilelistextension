package com.deveddy.seriesguidefilelistextension

import android.net.Uri

fun String.toUri(): Uri {
    return Uri.parse(this)
}
