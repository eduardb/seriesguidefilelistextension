package com.deveddy.seriesguidefilelistextension

import android.app.Application
import android.content.Context
import coil.Coil
import com.deveddy.seriesguidefilelistextension.infrastructure.di.AppComponent
import com.deveddy.seriesguidefilelistextension.infrastructure.di.AppComponentImpl
import com.google.android.material.color.DynamicColors

class ExtensionApplication : Application() {

    internal lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = AppComponentImpl.build(this)
        Coil.setImageLoader(appComponent.coilImageLoader())
        DynamicColors.applyToActivitiesIfAvailable(this)
    }
}

val Context.appComponent: AppComponent
    get() = (this.applicationContext as ExtensionApplication).appComponent
