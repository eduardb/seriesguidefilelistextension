package com.deveddy.seriesguidefilelistextension.ext

import androidx.activity.ComponentActivity
import androidx.activity.viewModels
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel

inline fun <reified VM : ViewModel> ComponentActivity.makeViewModel(
    crossinline block: (SavedStateHandle) -> VM
): Lazy<VM> = viewModels {
    object : AbstractSavedStateViewModelFactory(this, intent.extras) {
        override fun <T : ViewModel> create(
            key: String,
            modelClass: Class<T>,
            handle: SavedStateHandle
        ): T {
            assert(modelClass == VM::class.java)
            @Suppress("UNCHECKED_CAST")
            return block(handle) as T
        }
    }
}
