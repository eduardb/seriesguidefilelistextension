package com.deveddy.seriesguidefilelistextension.ext

import androidx.compose.ui.Modifier


/**
 * Example usage:
 * ```
 * Modifier.ifNotNull(onClick) { Modifier.clickable(onClick = it)}
 * ```
 */
inline fun <T : Any> Modifier.ifNotNull(
    value: T?,
    builder: (T) -> Modifier
): Modifier {
    return then(if (value != null) builder(value) else Modifier)
}

/**
 * Example usage:
 *
 * ```
 * Modifier.ifTrue(hasBorder) { Modifier.border(1.dp)}
 * ```
 */
inline fun Modifier.ifTrue(
    value: Boolean,
    builder: () -> Modifier
): Modifier {
    return then(if (value) builder() else Modifier)
}
