package com.deveddy.seriesguidefilelistextension.infrastructure.di

import android.content.Context
import com.deveddy.seriesguidefilelistextension.SeriesGuideDomainMapper
import com.deveddy.seriesguidefilelistextension.preferences.AppPreferences

class AppModule(context: Context) {

    internal val appPreferences = AppPreferences(context)

    internal val domainMapper = SeriesGuideDomainMapper(appPreferences)
}
