package com.deveddy.seriesguidefilelistextension.infrastructure.di

import android.content.Context
import coil.ImageLoader
import com.deveddy.seriesguidefilelistextension.BuildConfig
import com.deveddy.seriesguidefilelistextension.infrastructure.network.BasicAuthInterceptor
import okhttp3.Cache
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class NetworkModule(context: Context) {

    companion object {
        private const val DISK_CACHE_SIZE = 50L * 1024 * 1024 // 50MB
        private const val BASE_URL = "https://filelist.io/"
    }

    private val cache = Cache(context.cacheDir, DISK_CACHE_SIZE)

    private val client by lazy {
        OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(HttpLoggingInterceptor().apply {
                    if (BuildConfig.DEBUG) {
                        level = HttpLoggingInterceptor.Level.BODY
                    }
                })
                .addInterceptor(BasicAuthInterceptor(context))
                .build()
    }

    private val callFactory = object : Call.Factory {
        override fun newCall(request: Request): Call {
            // defer OkHttp initialization: https://www.zacsweers.dev/dagger-party-tricks-deferred-okhttp-init/
            return client.newCall(request)
        }
    }

    internal val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .callFactory(callFactory)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    internal val imageLoader = ImageLoader.Builder(context)
            .callFactory { callFactory }
            .build()
}
