package com.deveddy.seriesguidefilelistextension.infrastructure.di

import android.content.Context
import coil.ImageLoader
import com.deveddy.seriesguidefilelistextension.SeriesGuideDomainMapper
import com.deveddy.seriesguidefilelistextension.api.FilelistService
import com.deveddy.seriesguidefilelistextension.api.FilelistServiceModule
import com.deveddy.seriesguidefilelistextension.preferences.AppPreferences

interface AppComponent {

    fun filelistService(): FilelistService
    fun coilImageLoader(): ImageLoader
    fun appPreferences(): AppPreferences
    fun domainMapper(): SeriesGuideDomainMapper

}

class AppComponentImpl private constructor(context: Context) : AppComponent {

    private val appModule = AppModule(context)
    private val networkModule = NetworkModule(context)
    private val filelistServiceModule = FilelistServiceModule(networkModule.retrofit)

    companion object {
        fun build(applicationContext: Context): AppComponent {
            return AppComponentImpl(applicationContext)
        }
    }

    override fun filelistService() = filelistServiceModule.filelistService

    override fun coilImageLoader() = networkModule.imageLoader

    override fun appPreferences(): AppPreferences = appModule.appPreferences

    override fun domainMapper(): SeriesGuideDomainMapper = appModule.domainMapper

}
