package com.deveddy.seriesguidefilelistextension.infrastructure.network

import android.content.Context
import androidx.preference.PreferenceManager
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response

class BasicAuthInterceptor(context: Context) : Interceptor {

    private val preferences = PreferenceManager.getDefaultSharedPreferences(
            context
    )

    override fun intercept(chain: Interceptor.Chain): Response {
        val username = preferences.getString("username", null) ?: ""
        val passkey = preferences.getString("passkey", null) ?: ""
        val credentials = Credentials.basic(username, passkey)
        val request = chain.request()
        val authenticatedRequest = request.newBuilder()
                .header("Authorization", credentials).build()
        return chain.proceed(authenticatedRequest)
    }

}
