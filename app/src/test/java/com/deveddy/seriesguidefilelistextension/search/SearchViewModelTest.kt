package com.deveddy.seriesguidefilelistextension.search

import com.battlelancer.seriesguide.api.Episode
import com.battlelancer.seriesguide.api.Movie
import com.deveddy.seriesguidefilelistextension.SeriesGuideDomainMapper
import com.deveddy.seriesguidefilelistextension.api.FilelistService
import com.deveddy.seriesguidefilelistextension.api.TorrentDto
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.invoking
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldThrow
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.doThrow
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.verifyNoInteractions
import org.mockito.kotlin.whenever
import org.orbitmvi.orbit.test
import java.time.Instant
import java.util.*

@ExperimentalCoroutinesApi
class SearchViewModelTest {

    private companion object {
        const val SHOW_TITLE = "show title (year)"
        const val SHOW_SEASON = 3
        const val SHOW_EPISODE = 5
        const val QUALITY = "123p"
        const val MOVIE_TITLE = "movie title (year)"
        val MOVIE_RELEASE_DATE = Date.from(Instant.parse("2007-12-03T10:15:30.00Z"))
        const val EPISODE_NAME = "$SHOW_TITLE s${SHOW_SEASON}e${SHOW_EPISODE} $QUALITY"
        val episode: Episode = Episode.Builder()
            .showTitle(SHOW_TITLE)
            .season(SHOW_SEASON)
            .number(SHOW_EPISODE)
            .build()
        val movie: Movie = Movie.Builder()
            .title(MOVIE_TITLE)
            .releaseDate(MOVIE_RELEASE_DATE)
            .build()
    }

    private val mapper: SeriesGuideDomainMapper = mock() {
        on { name(episode) } doReturn EPISODE_NAME
        on { name(movie) } doReturn "$MOVIE_TITLE 2007 $QUALITY"
    }
    private val searchService: FilelistService = mock()
    private val navigator: Navigator = mock()

    @Test
    fun `throws if both movie and episode are null`() {
        invoking { makeVM(null, null) } shouldThrow IllegalArgumentException::class
    }

    @Test
    fun `throws if both movie and episode are not null`() {
        invoking { makeVM(episode, movie) } shouldThrow IllegalArgumentException::class
    }

    @Test
    fun `initializes with episode name`() = runTest {
        val vm = makeVM(episode, null)

        val initialState = vm.container.stateFlow.value

        initialState shouldBeEqualTo SearchState.Searching(EPISODE_NAME)
    }

    @Test
    fun `loads results if service call successful`() = runTest {
        val initialState = SearchState.Searching("test")
        whenever(searchService.searchTorrents(initialState.title)).doReturn(emptyList())

        val vm = makeVM(episode, null).test(initialState = initialState, isolateFlow = false)
        vm.runOnCreate()

        vm.assert(initialState) {
            states(
                { SearchState.ResultsLoaded(initialState.title, emptyList()) }
            )
        }
    }

    @Test
    fun `loads error if service call fails`() = runTest {
        val initialState = SearchState.Searching("test")
        whenever(searchService.searchTorrents(initialState.title)).doThrow(Error("Some error"))

        val vm = makeVM(episode, null).test(initialState = initialState, isolateFlow = false)
        vm.runOnCreate()

        vm.assert(initialState) {
            states(
                { SearchState.Error(initialState.title, "Some error") }
            )
        }
    }

    @Test
    fun `opens settings`() = runTest {
        val initialState = SearchState.Searching("test")
        val vm = makeVM(episode, null).test(initialState)

        vm.testIntent {
            onSettingsClicked()
        }

        verify(navigator).openSettings()
    }

    @Test
    fun `opens torrent successfully`() = runTest {
        val aTorrent: TorrentDto = mock()
        val initialState = SearchState.ResultsLoaded("test", listOf(aTorrent))
        val vm = makeVM(episode, null).test(initialState)
        whenever(navigator.open(aTorrent)).thenReturn(true)

        vm.testIntent {
            onResultClicked(aTorrent)
        }

        verify(navigator).open(aTorrent)

        vm.assert(initialState)
    }

    @Test
    fun `cannot open torrent`() = runTest {
        val aTorrent: TorrentDto = mock()
        val initialState = SearchState.ResultsLoaded("test", listOf(aTorrent))
        val vm = makeVM(episode, null).test(initialState)
        whenever(navigator.open(aTorrent)).thenReturn(false)

        vm.testIntent {
            onResultClicked(aTorrent)
        }

        vm.assert(initialState) {
            states({ initialState.copy(showingTorrentAppNotFoundDialog = true) })
        }
    }

    @Test
    fun `hides dialog to download torrent app`() = runTest {
        val initialState = SearchState.ResultsLoaded("test", showingTorrentAppNotFoundDialog = true)
        val vm = makeVM(episode, null).test(initialState)

        vm.testIntent {
            onTorrentAppNotFoundDialogDismissed()
        }

        vm.assert(initialState) {
            states({ initialState.copy(showingTorrentAppNotFoundDialog = false) })
        }
        verifyNoInteractions(navigator)
    }

    @Test
    fun `opens torrent app download search`() = runTest {
        val initialState = SearchState.ResultsLoaded("test", showingTorrentAppNotFoundDialog = true)
        val vm = makeVM(episode, null).test(initialState)

        vm.testIntent {
            onTorrentAppNotFoundDialogConfirmed()
        }

        vm.assert(initialState) {
            states({ initialState.copy(showingTorrentAppNotFoundDialog = false) })
        }
        verify(navigator).searchTorrentApp()
    }

    private fun makeVM(episode: Episode?, movie: Movie?) = SearchViewModel(
        mapper = mapper,
        searchService = searchService,
        navigator = navigator,
        episode = episode,
        movie = movie,
        savedStateHandle = mock(),
    )
}
