package com.deveddy.seriesguidefilelistextension

import com.battlelancer.seriesguide.api.Episode
import com.battlelancer.seriesguide.api.Movie
import com.deveddy.seriesguidefilelistextension.preferences.AppPreferences
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import java.time.Instant
import java.util.Date

class SeriesGuideDomainMapperTest {

    companion object {
        const val QUALITY = "123p"
        const val SYMBOLS = "()"
        val episode: Episode = Episode.Builder()
                .showTitle("show title (year)")
                .number(5)
                .season(3)
                .build()
        val movie: Movie = Movie.Builder()
                .title("movie title (year)")
                .releaseDate(Date.from(Instant.parse("2007-12-03T10:15:30.00Z")))
                .build()
    }

    private val mockPreferences: AppPreferences = mock() {
        on { stripSymbols() } doReturn SYMBOLS
        on { quality() } doReturn QUALITY
    }

    private val mapper = SeriesGuideDomainMapper(mockPreferences)

    @Test
    fun mapsEpisode() {
        val expectedQuery = "https://filelist.io/browse.php?search=show+title+year+s03e05+123p"

        mapper.mapToQuery(episode) shouldBeEqualTo expectedQuery
    }

    @Test
    fun mapsMovie() {
        val expectedQuery = "https://filelist.io/browse.php?search=movie+title+year+2007+123p"

        mapper.mapToQuery(movie) shouldBeEqualTo expectedQuery
    }

    @Test
    fun nameEpisode() {
        val expectedName = "show title year s03e05 123p"

        mapper.name(episode) shouldBeEqualTo expectedName
    }

    @Test
    fun nameMovie() {
        val expectedName = "movie title year 2007 123p"

        mapper.name(movie) shouldBeEqualTo expectedName
    }
}
